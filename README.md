# Materi Training #

## Hari ke 1 ##

### Sesi 1 ###

- Arsitektur MicroService - Cloud Native App
- 12 Factor
- Ephemeral Resources

### Sesi 2 ###

- Setup Project dengan Spring Boot - Deployment ke Cloud
- Continuous Delivery

## Hari ke 2 ##

### Sesi 3 ###

- Managed Configuration
- Spring Cloud Config Server - Spring Cloud Config Client

### Sesi 4 ###

- Service Registry & Discovery - Spring Cloud Eureka Server
- Spring Cloud Eureka Client
- Config First vs Discovery First

## Hari ke 3 ## 

### Sesi 5 ###

- Aplikasi Web sebagai MicroServices
- Client Side Load Balancer dengan Ribbon
- Declarative REST client dengan Feign
- Circuit Breaker dengan Hystrix
- Monitoring Health dengan Hystrix Dashboard

### Sesi 6 ###

- Message Driven Microservices
- Spring Cloud Stream
- Implementasi Messaging dengan RabbitMQ/Kafka

## Hari ke 4 ##

### Sesi 7 ###

- Microservices Security
- Konsep OAuth
- Berbagai OAuth Grant Type
- Membuat OAuth Authorization Server

### Sesi 8 ###

- Proteksi Microservices dengan OAuth Resource Server - Login with Google/Facebook
- Security antar service dengan Client Credential

## Hari ke 5 ###

### Sesi 9 ###

- Migrasi dari monolith ke microservices 
- Strangulation pattern
- Router dan Filter dengan Zuul

### Sesi 10 ###

- Distributed Tracing dan Monitoring
- Spring Cloud Sleuth & Zipkin
- Pengumpulan data dan analisa log dengan ELK

## Hari ke 6 ##

- Load Balancer dengan Nginx 
- Load Balancer dengan Zuul